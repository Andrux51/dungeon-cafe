# Dungeon Café

### Description

A rogue-like game with a non-combat side of the game focused on running a cafe.

Food API used: ```http://platform.fatsecret.com/api/Default.aspx?method=foods.search&format=json```


## Adventure

- Allow option for random character creation or to be able to choose at character creation time
- At world generation, randomize enemy levels and stats (maybe dragons are level 1 and goblins level 50)
- Some ingredients drop from enemies, some are found on the ground
- Encourage exploring the entire map

## Simulation

- Serve customers (meet special requests for special rewards)
- Upgrade cooking facilities to make advanced recipes and expand space
- Serve adventurer to improve stats