require "rubygems"
require "gosu"

require "./game_window"

# use this for code executed at runtime (to avoid window opening in test runner)
if $0 == __FILE__
    $window = GameWindow.new
    $window.show
end
