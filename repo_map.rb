require "json"

# JSON.parse(json, { :symbolize_names => true })
class RepoMap
    attr_accessor :tiles
    def initialize
    end

    def build_rand_map(size_x = 100, size_y = 50)
        @tiles = []
        tile_map = { :x => size_x, :y => size_y }

        tilesets = []

        tileset_grassland = Gosu::Image.load_tiles("./images/tilesets/grassland.png", 32, 32, :tileable => true)
        tilesets.push(tileset_grassland)

        tile_map[:y].times { |y|
            tile_map[:x].times { |x|
                tile = { coord: {x: x - size_x / 4, y: y - size_y / 4, z: 1}, data: {} }

                tileset = tilesets[$random.rand(tilesets.length)]
                tile[:image] = tileset[$random.rand(tileset.length)]

                @tiles.push(tile)
            }
        }
    end
end
