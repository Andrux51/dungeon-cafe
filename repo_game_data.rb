require "json"
require "./player"

class RepoGameData
    attr_accessor :player, :save_path

    def initialize
        @save_path = Dir.home + "/Documents/My Games/DungeonCafe/"
        @saves = { :folder => "saves", :extension => "sav" }
        @players = { :folder => "players", :extension => "player" }
        @cafes = { :folder => "cafes", :extension => "cafe" }

        @player = Player.new
    end

    def create_dir_if_not_exists(subdir = "")
        dir_path = @save_path + (!subdir.empty? ? subdir + "/" : "")

        if !Dir.exist? dir_path
            Dir.mkdir(dir_path)
        end
    end

    def save_game_file(data, id, extension, ref = false)
        if ref
            file_to_save = @save_path + id + ".sav"
        else
            file_to_save = @save_path + id + "/" + extension + ".dat"
        end
        temp_save_file = file_to_save + "_t"

        create_dir_if_not_exists("..")
        create_dir_if_not_exists
        create_dir_if_not_exists(id)

        if File.exist? file_to_save
            IO.write(temp_save_file, data)
            File.rename(temp_save_file, file_to_save)
        else
            IO.write(file_to_save, data)
        end
    end

    def save_player_file
        save_game_file(JSON.dump(@player.json), @player.id, @players[:extension])
    end

    def save_refs_file
        timestamp = Time.now().localtime
        ref_data = {
            :id => @player.id,
            :name => @player.name,
            :game_state => $window.game_state,
            :timestamp => timestamp.strftime("%s%L"),
            :moment => timestamp.strftime("%A, %-e %b, \'%y \@ %-l:%M:%S %P")
        }

        save_game_file(JSON.dump(ref_data), ref_data[:id], @saves[:extension], true)
    end

    def save_game_data
        save_player_file

        save_refs_file
    end

    def load_ref_files
        if Dir.exist? @save_path
            ref_files = Dir.entries(@save_path)
            Dir.glob("*.#{@saves[:extension]}")
            ref_files.keep_if { |file| file.end_with? "." + @saves[:extension] }

            refs = []
            ref_files.each { |file|
                json = IO.read(@save_path + file)
                refs.push(JSON.parse(json, { :symbolize_names => true }))
            }

            refs.sort_by!{ |ref| ref[:timestamp] }.reverse!

            return refs
        else
            return []
        end
    end

    def load_game_data(id)
        # NOTE: this will also load cafe and other data
        load_player_data(id)
    end

    def load_player_data(id)
        file_to_load = @save_path + id + "/player.dat"

        if File.exist? file_to_load
            json = IO.read(file_to_load)
            @player.json = JSON.parse(json, { :symbolize_names => true })
            if @player.json[:id] == id
                @player.load_from_json
            end
        end
    end
end
