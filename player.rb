require "securerandom"

class Player
    attr_accessor :image, :name, :position, :id, :json

    def initialize(image = "images/player.png", name = "Pabbe")
        # https://www.libgosu.org/rdoc/frames.html#!file.README.html
        @player_image = image
        @image = Gosu::Image.new(image)
        @name = name
        @position = { :x => 0, :y => 0, :z => 10 }
        @id = SecureRandom.uuid

        save_to_json
    end

    def update
        # update_movement

        save_to_json
    end

    def draw
        @image.draw($screen[:width] / 2 - @image.width, $screen[:height] / 2 - @image.height, @position[:z])
    end

    def save_to_json
        @json = {
            :image => @player_image,
            :name => @name,
            :position => @position,
            :id => @id
        }
    end

    def load_from_json
        @image = Gosu::Image.new(@json[:image])
        @name = @json[:name]
        @position = @json[:position]
        @id = @json[:id]
    end

    def update_movement
        # move down
        if $key_state.press_keys([Gosu::KbDown, Gosu::KbS, Gosu::KbNumpad2])
            move(0, @image.height)
        end

        # move up
        if $key_state.press_keys([Gosu::KbUp, Gosu::KbW, Gosu::KbNumpad8])
            move(0, -@image.height)
        end

        # move left
        if $key_state.press_keys([Gosu::KbLeft, Gosu::KbA, Gosu::KbNumpad4])
            move(-@image.width, 0)
        end

        # move right
        if $key_state.press_keys([Gosu::KbRight, Gosu::KbD, Gosu::KbNumpad6])
            move(@image.width, 0)
        end

        # move down-right
        if $key_state.press_key Gosu::KbNumpad3
            move(@image.width, @image.height)
        end

        # move down-left
        if $key_state.press_key Gosu::KbNumpad1
            move(-@image.width, @image.height)
        end

        # move up-left
        if $key_state.press_key Gosu::KbNumpad7
            move(-@image.width, -@image.height)
        end

        # move up-right
        if $key_state.press_key Gosu::KbNumpad9
            move(@image.width, -@image.height)
        end
    end

    def move(x, y)
        # bind player inside screen borders
        if @position[:x] >= (0 - x) && @position[:x] <= ($screen[:width] - @image.width - x)
            @position[:x] += x
        end
        if @position[:y] >= (0 - y) && @position[:y] <= ($screen[:height] - @image.height - y)
            @position[:y] += y
        end
    end

    def collect_item
        # TODO
    end

    def use_item
        # TODO
    end
end
